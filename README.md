# dpc_practicals


## Goals
In the foreground, it is not exclusively the correct completion of the missing modules, 
but rather the definition of suitable experiments to determine dependencies and rules with regard to different configurations.

## Preliminaries
To complete the tasks, you should checkout this project. 
This contains the code skeleton and data sets for using the record linkage system.
You should integrate the modules and data sets into your Python project.

Before implementation, you should familiarize yourself with the individual modules and the structure for the various tasks.
For a more detailed understanding, you can run ```recordLinkage.py```. It uses the provided data records and already implemented functions. 
This makes the output of the different modules comprehensible. To test the program for correctness, 
it is recommended to use the data records without impurities, the 
```clean-A-1000.csv``` and ```clean-B-1000.csv``` that can be found in the ```datasets``` folder. The other data records can then be used.


## Tasks
1. Blocking
2. Comparsion
3. Classification
4. Evaluation
